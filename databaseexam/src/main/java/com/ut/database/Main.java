package com.ut.database;

//import com.mysql.fabric.jdbc.FabricMySQLDriver;
//import sun.security.util.Password;

import java.sql.*;

public class Main {

    private static final String URL = "jdbc:mysql://localhost:3306/mydbtest";
    private final static String URLFIXED =
            "jdbc:mysql://localhost:3306/mydbtest?useUnicode=true&useSSL=true&useJDBCCompliantTimezoneShift=true" +
                    "&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "1234";

    public  static void main(String[] args)  {
        Connection connection;

        /**
         <dependency>
         <groupId>mysql</groupId>
         <artifactId>mysql-connector-java</artifactId>
         <version>6.0.2</version>
         </dependency>
         * The driver is automatically registered via the SPI
         * and manual loading of the driver class is generally unnecessary.
         * Драйвер автоматически регистрируется через SPI и теперь не нужно вручную его регистрировать*/

        try {
          //  Driver driver = new FabricMySQLDriver(); // в новых версиях эта конструкция уже не поддерживается альтернатива в следующем комите
           // DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URLFIXED, USERNAME, PASSWORD);

            // Проверяем успешность подключения к БД
            if (!connection.isClosed()) {
                System.out.println("Соединение с БД установлено!");
            }

            //сюда будем прописывать запросы
            try {
                Statement statement = connection.createStatement();
             //   statement.execute("INSERT INTO animal(anim_name, anim_desc) VALUES ('name', 'desc');");
            // int res = statement.executeUpdate("UPDATE animal SET anim_name='New Name' WHERE ID=1;");
           //  System.out.println(res);
              //  ResultSet resultSet = statement.executeQuery("SELECT * FROM animal");

                // далее пакетная обработка

                statement.addBatch("INSERT INTO animal(anim_name, anim_desc) VALUES ('batch1', 'desc')");
                statement.addBatch("INSERT INTO animal(anim_name, anim_desc) VALUES ('batch2', 'desc')");
                statement.addBatch("INSERT INTO animal(anim_name, anim_desc) VALUES ('batch3', 'desc')");

                //выполняем пакет
                statement.executeBatch();
                //отчищаем пакет от прошлых запросов
                statement.clearBatch();



                statement.close();

                boolean status;
                if (statement.isClosed()) status = true;
                else status = false;
                //за однопроверим закрыт ли наш стетемент
                if (status) {
                    System.out.println("Статемент закрыт");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            connection.close();

            if (connection.isClosed()) {
                System.out.println("Соединение с БД Закрыто!");
            }


        } catch (SQLException e) {
            System.err.println("Не удалось загрузить класс драйвера!");
        }



    }


}
