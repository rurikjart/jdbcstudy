package com.ut.db.ps;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBEngine {

    private final  String HOST =
            "jdbc:mysql://localhost:3306/mydbtest?useUnicode=true&useSSL=true&useJDBCCompliantTimezoneShift=true" +
                    "&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private   String USERNAME = "root";
    private   String PASSWORD = "1234";

    private Connection connection;

    public DBEngine() {

        try {
            connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection() {
        return connection;
    }

}
