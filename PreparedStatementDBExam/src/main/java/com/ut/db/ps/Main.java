package com.ut.db.ps;


import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.sql.*;
import java.util.Calendar;


public class Main {

    private static final String INSERT_NEW="INSERT INTO dish VALUES(?,?,?,?,?,?,?)";
    private static final String GET_ALL="SELECT * FROM dish";
    private static final String DEL_ALL="DELETE FROM dish";



    public  static void main (String[] args) throws SQLException {
      //  Connection connection = null;
      //  PreparedStatement preparedStatement = null;

       // DBEngine engine = new DBEngine();

        try {
            // Подготовка к подключению
         // connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
         // preparedStatement = connection.prepareStatement(INSERT_NEW);

          RpreparedStatement rprepared = new RpreparedStatement();
          PreparedStatement  ps = rprepared.getStatement(INSERT_NEW);


          //вставка данных

            ps.setInt(1, 8);
            ps.setString(2, "Inserted title");
            ps.setString(3, "Inserted desc");
            ps.setFloat(4, 0.2f);
            ps.setBoolean(5, true);
            ps.setDate(6, new Date(Calendar.getInstance().getTimeInMillis()));
            ps.setBlob(7, new FileInputStream("smile.jpg"));

            //Выполняем запрос на добавление
            ps.execute();

            rprepared.Close();
        }

        catch (SQLException | FileNotFoundException e) {
            e.printStackTrace(); }


        //вывод данных из базы
        try {
            // Подготовка к подключению
            RpreparedStatement rpreparedGetAll = new RpreparedStatement();
            PreparedStatement  psga = rpreparedGetAll.getStatement(GET_ALL);

           ResultSet res = psga.executeQuery();


                   while (res.next()) {
                       //Достаем в переменные а потом засунем в поля объеката только учебный пример
                       int id = res.getInt("id");
                       String title = res.getString("title");
                       String desc = res.getString("description");
                       float rating = res.getFloat("rating");
                       boolean published = res.getBoolean("published");
                       Date date = res.getDate("created");
                       Blob icon = res.getBlob("icon");




                       //создаем объект Dish для записи в него
                       Dish dish = new Dish();

                       //пишем сеттерами
                       dish.setId(id);
                       dish.setTitle(title);
                       dish.setDescription(desc);
                       dish.setRating(rating);
                       dish.setPublished(published);
                       dish.setCreated(date);
                       dish.setIcon(icon);

                       System.out.println(dish);

                   }

            rpreparedGetAll.Close();


        } catch (SQLException e) {
            e.printStackTrace();
        }


        //удаление всех записей в таблице dish

        //вывод данных из базы
        try {
            // Подготовка к подключению
            RpreparedStatement rpreparedDelAll = new RpreparedStatement();
            PreparedStatement  psga = rpreparedDelAll.getStatement(DEL_ALL);

            //непосредственно операция удаления

            psga.executeUpdate();
            rpreparedDelAll.Close();



        }

        catch (SQLException e) {
            e.printStackTrace();
        }
        
    }

}
