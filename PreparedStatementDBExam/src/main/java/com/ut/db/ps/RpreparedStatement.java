package com.ut.db.ps;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RpreparedStatement {


    private  PreparedStatement preparedStatement;
    private Connection connection;





    public RpreparedStatement() {

    }




    public PreparedStatement getStatement(String statement) throws SQLException {

        DBEngine engine = new DBEngine();
        connection = engine.getConnection();

        preparedStatement = connection.prepareStatement(statement);
        return preparedStatement;

    }

    public void Close() throws SQLException {

        preparedStatement.close();
        connection.close();


    }
}
