package com.ut.db.ps;



import java.sql.Blob;
import java.util.Date;

public class Dish {

    private int id;
    private String title;
    private String description;
    private Float rating;
    private Boolean published;
    private Date created;
    private Blob icon;

    public Dish() {
    }

    public Dish(String title, String description, Float rating, Boolean published, Date created, Blob icon) {
        this.title = title;
        this.description = description;
        this.rating = rating;
        this.published = published;
        this.created = created;
        this.icon = icon;
    }

    public Dish(int id, String title, String description, Float rating, Boolean published, Date created, Blob icon) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.rating = rating;
        this.published = published;
        this.created = created;
        this.icon = icon;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setIcon(Blob icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
      
            return "Dish{" +
                    "id=" + id +
                    ", title='" + title + '\'' +
                    ", description='" + description + '\'' +
                    ", rating=" + rating +
                    ", published=" + published +
                    ", created=" + created +
                    ", icon=" + icon +
                    '}';
       
    }
}
